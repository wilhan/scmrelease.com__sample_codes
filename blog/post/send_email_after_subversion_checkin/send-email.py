import sys
import smtplib
import subprocess
 
repos =  sys.argv[1]
revision = sys.argv[2]
 
# to and from address
 
fromaddr = 'from_email@gmail.com'
toaddrs  = 'to_email@gmail.com'
 
#
cmd = 'svnlook tree --revision ' + revision + ' ' + repos
svn_tree = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True).stdout.read()
 
cmd = 'svnlook author --revision ' + revision + ' ' + repos
svn_author = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True).stdout.read()
 
cmd = 'svnlook changed --revision ' + revision + ' ' + repos
svn_changed = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True).stdout.read()
 
cmd = 'svnlook date --revision ' + revision + ' ' + repos
svn_date = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True).stdout.read()
 
# Writing the message (this message will appear in the email)
 
subject = 'New Subversion update'
 
body = 'svn checkin information \n' + \
        '\n' + '==========\n' + \
       'repos= ' + repos + \
       '\n' + '==========\n' + \
       'revision= ' + revision + \
       '\n' + '==========\n' + \
       'svn_author= ' + svn_author + \
       '\n' + '==========\n' + \
       'svn_date= ' + svn_date + \
       '\n' + '==========\n' + \
       'svn_changed= ' + svn_changed + \
       '\n' + '==========\n' + \
       'svn_tree= ' + svn_tree
 
msg = 'Subject: %s\n\n%s' % (subject, body)
 
# Gmail Login
 
username = 'USERNAME@gmail.com'
password = 'PASSWORD'
 
# Sending the mail
 
server = smtplib.SMTP('smtp.gmail.com:587')
server.starttls()
server.login(username,password)
server.sendmail(fromaddr, toaddrs, msg)
server.quit()